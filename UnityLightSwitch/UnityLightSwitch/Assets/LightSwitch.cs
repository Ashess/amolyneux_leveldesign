﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightSwitch : MonoBehaviour
{
    public TriggerListener trigger;
    public Image cursorImage;
    public Light spotLight;
    public AudioSource audioSource;
    public Animation anim;

    //set up variable - get reference to light
    //set up variable - get reference to sound
    //set up variable - get reference to animation

	// Use this for initialization
	void Start ()
    {
        cursorImage.enabled = false;
        audioSource = GetComponent<AudioSource>();
        anim = GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Debug.Log("Update was called");
    }

    void OnMouseOver()
    {
        if(trigger.playerEntered == true)
        {
            if(cursorImage.enabled != true)
            {
                cursorImage.enabled = true;
            }
           
        }
        else
        {
            cursorImage.enabled = false;
        }
    }
    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
        }
    }
    private void OnMouseDown()
    {
        if (trigger.playerEntered == true)
        {
            audioSource.Play();

            anim.Stop();
            anim.Play();

            Debug.Log("Switch Pressed");
            if (spotLight.intensity > 0)
            {
                spotLight.intensity = 0f;
            }
            else
            {
                spotLight.intensity = 3f;
            }

        }
    }


}




