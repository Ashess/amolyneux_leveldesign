﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class KeypadDoor : MonoBehaviour {

    public TriggerListener_Com keypadDoorTrigger;
    public Image cursorImage;
    public Light doorLight;
    public bool unlocked;
    public CameraShake shake;
    public AudioSource roar;
    public Image blackScreen;
    public GameObject player;

    private void Update()
    {
        if (unlocked == true)
        {
            doorLight.color = (Color.green);
        }
    }

    void OnMouseOver()
    {
        if (keypadDoorTrigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;

            }
        }
        else
        {
            cursorImage.enabled = false;
        }

    }
    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;

        }
    }
    void OnMouseDown()
    {
        if (keypadDoorTrigger.playerEntered == true)
        {

            if (unlocked == true)
            {
                shake.shouldShake = true;
                roar.Play();
                Invoke("endScreen", 1);
                Invoke("endMenu", 4);
            }
        }
    }
    void endScreen()
    {
        blackScreen.enabled = true;
    }
    void endMenu()
    {
        player.GetComponent<FirstPersonController>().enabled = false;
        player.GetComponent<FirstPersonController>().SetMouseCursor(false);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        SceneManager.LoadScene(2);
    }
   
}
