﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorTwo : MonoBehaviour
{
    public TriggerListener_Com doorTwoTrigger;
    public Image cursorImage;
    public keycard keycard;
    public float doorOpenAmount;
    public Transform doorTran;
    public float speed = 5f;
    public Light doorLightOne;
    public Light doorLightTwo;

    void OnMouseOver()
    {
        if (doorTwoTrigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;

            }
        }
        else
        {
            cursorImage.enabled = false;
        }

    }
    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;

        }
    }
    void OnMouseDown()
    {
        if (keycard.keycardCollected == true)
        {
            StopCoroutine("DoorOpen");
            StartCoroutine("DoorOpen");
            doorLightOne.color = (Color.green);
            doorLightTwo.color = (Color.green);
        }

    }
    IEnumerator DoorOpen()
    {
        float xPos = doorTran.localPosition.x;
        while (xPos < 0.98f)
        {
            xPos = Mathf.Lerp(doorTran.localPosition.x, doorOpenAmount, Time.deltaTime * speed);
            doorTran.localPosition = new Vector3(xPos, 0, 0);
            yield return null;
        }
        doorTran.localPosition = new Vector3(doorOpenAmount, doorTran.localPosition.y, doorTran.localPosition.z);
        yield return null;
    }
}
