﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using System.Text.RegularExpressions;

public class Keypad : MonoBehaviour
{
    public TriggerListener_Com keypadTrigger;
    public Image cursorImage;
    public bool keypadScreen;
    public string input;
    public string password = "2313";
    public bool mouseDown;
    public bool unlocked;
    public GameObject player;
    bool useKeypad = false;

    void Update()
    {
        if (input == password)
        {
            Debug.Log("password done");
        }
    }

    void OnMouseOver()
    {
        if (keypadTrigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;
            }
            
        }

    }
    void OnMouseExit()
    {
        /*if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
            keypadScreen = false;
            input = "";

        }*/
    }

    void OnGUI()
    {
        if (keypadTrigger.playerEntered == true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                keypadScreen = true;
                Cursor.lockState = CursorLockMode.Locked;
                player.GetComponent<FirstPersonController>().enabled = false;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            else
            {

            }
        }

        if (keypadScreen)
        {
            GUI.Box(new Rect(0, 0, 320, 350), "");
            GUI.Box(new Rect(5, 5, 310, 25), input);
           

            if (GUI.Button(new Rect(5, 35, 100, 100), "1"))
            {
                input = input + "1";
            }

            if (GUI.Button(new Rect(110, 35, 100, 100), "2"))
            {
                input = input + "2";
            }

            if (GUI.Button(new Rect(215, 35, 100, 100), "3"))
            {
                input = input + "3";
            }

            if (GUI.Button(new Rect(5, 140, 100, 100), "4"))
            {
                input = input + "4";
            }

            if (GUI.Button(new Rect(110, 140, 100, 100), "5"))
            {
                input = input + "5";
            }

            if (GUI.Button(new Rect(215, 140, 100, 100), "6"))
            {
                input = input + "6";
            }

            if (GUI.Button(new Rect(5, 245, 100, 100), "7"))
            {
                input = input + "7";
            }

            if (GUI.Button(new Rect(110, 245, 100, 100), "8"))
            {
                input = input + "8";
            }

            if (GUI.Button(new Rect(215, 245, 100, 100), "9"))
            {
                input = input + "9";
            }

        }
    }
    

}
    
  

   
    

