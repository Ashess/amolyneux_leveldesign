﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorOne : MonoBehaviour
{
    public TriggerListener_Com doorOneTrigger;
    public Image cursorImage;
    public FuseBox fuse;
    public float doorOpenAmount;
    public Transform doorTran;
    public float speed = 5f;
    public Text popUP;


  
    void OnMouseOver()
    {
        if (doorOneTrigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;

            }
        }
        else
        {
            cursorImage.enabled = false;
        }

    }
    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;

        }
    }
    void OnMouseDown()
    {
        if (doorOneTrigger.playerEntered == true)
        {
            if (fuse.fusePlaced == false)
            {
                Debug.Log("Fuse not returned");
            }
            else if (fuse.fusePlaced == true)
            {

                Debug.Log("Door is Open");
                StopCoroutine("DoorOpen");
                StartCoroutine("DoorOpen");

            }


        }
    }
    IEnumerator DoorOpen()
    {
        float xPos = doorTran.localPosition.x;
        while (xPos < 0.98f)
        {
            xPos = Mathf.Lerp(doorTran.localPosition.x, doorOpenAmount, Time.deltaTime * speed);
            doorTran.localPosition = new Vector3(xPos, 0, 0);
            yield return null;
        }
        doorTran.localPosition = new Vector3(doorOpenAmount, doorTran.localPosition.y, doorTran.localPosition.z);
        yield return null;
    }
}

