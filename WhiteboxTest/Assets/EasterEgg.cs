﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EasterEgg : MonoBehaviour {
    public RawImage popUp;
    public AudioSource type;
    public TriggerListener_Com terminalTrigger;


    private void OnMouseDown()
    {
        if (terminalTrigger.playerEntered == true)
        {
            if (popUp.enabled == false)
            {
                popUp.enabled = true;

            }
            type.Stop();
            type.Play();
        }
        
    }
    private void OnMouseExit()
    {
        popUp.enabled = false;
    }
}
