﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FuseBox : MonoBehaviour {
    public TriggerListener_Com fuseBoxTrigger;
    public Image cursorImage;
    public FuseKey key;
    MeshRenderer meshRend;
    public Light doorLightOne;
    public Light doorLightTwo;
    public bool fusePlaced = false;
    public AudioSource click;
    public AudioSource buzz;
    public bool interacted;
        
	// Use this for initialization
	void Start ()
    {
        meshRend = GetComponent<MeshRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnMouseOver()
    {
        if (fuseBoxTrigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;

            }
        }
        else
        {
            cursorImage.enabled = false;
        }

    }
    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;

        }
    }
    void OnMouseDown()
    {
        if (interacted == false)
        {
            if (fuseBoxTrigger.playerEntered == true)
            {
                if (key.keyPickedUp == false)
                {
                    Debug.Log("Fuse Needed");
                }
                else if (key.keyPickedUp == true)
                {
                    Debug.Log("Fuse Got");
                    meshRend.enabled = true;
                    fusePlaced = true;
                    doorLightOne.color = (Color.green);
                    doorLightTwo.color = (Color.green);
                    click.Play();
                    buzz.Stop();
                    interacted = true;
                }


            }

        }


       
       
    }
}
