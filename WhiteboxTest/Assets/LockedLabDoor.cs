﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LockedLabDoor : MonoBehaviour {

    public TriggerListener_Com doorOneTrigger;
    public Image cursorImage;
    public Light doorLight;
    public AudioSource roar;
    public AudioSource scream;
    public bool interacted;

    // Use this for initialization
    void Start()
    {


    }
    void OnMouseOver()
    {
        if (doorOneTrigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;

            }
        }
        else
        {
            cursorImage.enabled = false;
        }

    }
    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;

        }
    }
    void OnMouseDown()
    {
        if (interacted == false)
        {
            doorLight.color = (Color.red);
            roar.Play();
            scream.Play();
            interacted = true;

        }
        else if (interacted == true)
        {
            Debug.Log("Already used");
        }

       

    }


   
}
