﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cheats : MonoBehaviour {
    public TriggerListener_Com trigger;
    public RawImage popUp;

    private void OnMouseDown()
    {
        if (trigger.playerEntered == true)
        {
            if (popUp.enabled == false)
            {
                popUp.enabled = true;

            }
           
        }

    }
    private void OnMouseExit()
    {
        popUp.enabled = false;
    }
}
