﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;


public class Briefcase : MonoBehaviour
{
    public TriggerListener_Com trigger;
    public Image cursorImage;
    public bool unlocked;
    public MeshRenderer briefMesh;
    public MeshCollider briefCol;
    public GameObject player;
    public string briefcaseComb = "1603";
    public string fieldInput;

    public InputField briefcasePassword;
    public Text inputText;
    public Image inputImage;
    public Text placeholderInput;
    bool useKeyPad = false;
    public AudioSource unlockClick;
    public AudioSource unlockFail;

    void Update()
    {
        fieldInput = briefcasePassword.text;
        if (Input.GetKey(KeyCode.Return))
        {
            Debug.Log("enter");
            if (fieldInput == briefcaseComb)
            {
                unlocked = true;
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                briefcasePassword.enabled = false;
                inputText.enabled = false;
                inputImage.enabled = false;
                placeholderInput.enabled = false;
                player.GetComponent<FirstPersonController>().enabled = true;
                player.GetComponent<FirstPersonController>().SetMouseCursor(true);
                unlockClick.Stop();
                unlockClick.Play();
                

            }
            else
            {
                briefcasePassword.text = "";
                fieldInput = "";
                briefcasePassword.Select();
                briefcasePassword.ActivateInputField();
                unlockFail.Stop();
                unlockFail.Play();
            }
        }
        if (Input.GetKey(KeyCode.KeypadEnter))
        {
            if (fieldInput == briefcaseComb)
            {
                unlocked = true;
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                briefcasePassword.enabled = false;
                inputText.enabled = false;
                inputImage.enabled = false;
                placeholderInput.enabled = false;
                player.GetComponent<FirstPersonController>().enabled = true;
                player.GetComponent<FirstPersonController>().SetMouseCursor(true);
                unlockClick.Stop();
                unlockClick.Play();


            }
            else
            {
                briefcasePassword.text = "";
                fieldInput = "";
                briefcasePassword.Select();
                briefcasePassword.ActivateInputField();
                unlockFail.Stop();
                unlockFail.Play();
            }
        }
    }


    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;
            }
        }
        else
        {
            cursorImage.enabled = false;
        }
    }
    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;

            //player.GetComponent<CharacterController>().enabled = true;
        }

    }
    void OnMouseDown()
    {
        if (unlocked == false)
        {

            //briefcasePassword.Select();
            //briefcasePassword.ActivateInputField();
            //briefcasePassword.enabled = true;
            //inputText.enabled = true;
            //inputImage.enabled = true;
            //placeholderInput.enabled = true;
            Debug.Log("Shit Happen");

        }
        else if (unlocked == true)
        {
            briefMesh.enabled = false;
            briefCol.enabled = false;

        }

        useKeyPad = !useKeyPad;
        if (useKeyPad)
        {
            //player.GetComponent<FirstPersonController>().SetMouseCursor(true);
            player.GetComponent<FirstPersonController>().enabled = false;
            briefcasePassword.enabled = true;
            inputText.enabled = true;
            inputImage.enabled = true;
            placeholderInput.enabled = true;
            briefcasePassword.Select();


        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            briefcasePassword.enabled = false;
            inputText.enabled = false;
            inputImage.enabled = false;
            placeholderInput.enabled = false;
            player.GetComponent<FirstPersonController>().enabled = true;
            player.GetComponent<FirstPersonController>().SetMouseCursor(true);
        }

    }
       
}


