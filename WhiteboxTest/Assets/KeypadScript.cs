﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class KeypadScript : MonoBehaviour {

    public TMP_InputField keypadScreen;
    public string inputText;

    private void Update()
    {
        keypadScreen.text = inputText;
    }


    public void numOne()
    {
        inputText = inputText + "1";
    }
    public void numTwo()
    {
        inputText = inputText + "2";
    }
    public void numThree()
    {
        inputText = inputText + "3";
    }
    public void numFour()
    {
        inputText = inputText + "4";
    }
    public void numFive()
    {
        inputText = inputText + "5";
    }
    public void numSix()
    {
        inputText = inputText + "6";
    }
    public void numSeven()
    {
        inputText = inputText + "7";
    }
    public void numEight()
    {
        inputText = inputText + "8";
    }
    public void numNine()
    {
        inputText = inputText + "9";
    }
    public void numZero()
    {
        inputText = inputText + "0";
    }
    public void Clear()
    {
        inputText = "";
    }
}
