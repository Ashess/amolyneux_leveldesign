﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Terminal : MonoBehaviour {
    public TriggerListener_Com terminalTrigger;
    public Image cursorImage;
    public RawImage popUp;
    public AudioSource type;
    
 

    private void OnMouseOver()
    {
        if (terminalTrigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;
            }  
        }
        else
        {
            cursorImage.enabled = false;
        }
    }
    private void OnMouseExit()
    {
        
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
            
        }
        popUp.enabled = false;
    }
    private void OnMouseDown()
    {
        
        if (popUp.enabled == false)
        {
            popUp.enabled = true;
           
        }
        type.Stop();
        type.Play();
    }
}

