﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FuseKey : MonoBehaviour
{
    public TriggerListener_Com fuseTrigger;
    public Image cursorImage;
    MeshRenderer meshRend;
    public bool keyPickedUp = false;
    public AudioSource collect;


	// Use this for initialization
	void Start ()
    {
        meshRend = GetComponent<MeshRenderer>();
	}
	
    void OnMouseOver()
    {
        if (fuseTrigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;

            }
        }
        else
        {
            cursorImage.enabled = false;
        }
       
           

       
    
    }
    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;

        }
    }
    void OnMouseDown()
    {
        if (fuseTrigger.playerEntered == true)
        {
            keyPickedUp = true;
            meshRend.enabled = false;
            GetComponent<MeshCollider>().enabled = false;
            collect.Play();
        }

        
    }
}
