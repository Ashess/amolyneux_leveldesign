﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;
using TMPro;

public class KeypadPuzzle : MonoBehaviour {

    public TriggerListener_Com keypadDoorTrigger;
    public Image cursorImage;
    public Light doorLight;
    public string password;
    public CameraShake shake;
    public AudioSource roar;
    public Image blackScreen;
    public GameObject player;
    bool useKeyPad = false;
    public GameObject keypad;
    public KeypadScript input;
    public KeypadDoor door;
    public AudioSource correct;
    public AudioSource incorrect;
    public bool solved = false;

    public void Start()
    {
        password = "4952";
    }


    void OnMouseOver()
    {
        if (keypadDoorTrigger.playerEntered == true)
        {
            if (useKeyPad == false)
            {
                if (cursorImage.enabled == false)
                {
                    cursorImage.enabled = true;

                }
            }
           
        }
        else
        {
            cursorImage.enabled = false;
        }

    }
    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;

        }
    }
    void OnMouseDown()
    {
        if (solved == false)
        {
            useKeyPad = !useKeyPad;
            if (useKeyPad)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                player.GetComponent<FirstPersonController>().SetMouseCursor(false);
                player.GetComponent<FirstPersonController>().enabled = false;
                keypad.SetActive(true);
                cursorImage.enabled = false;

            }
            else
            {
                
            }
        }
        
    }
    public void Submit()
    {
        if (password == input.inputText)
        {
            correct.Play();
            door.unlocked = true;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            player.GetComponent<FirstPersonController>().enabled = true;
            player.GetComponent<FirstPersonController>().SetMouseCursor(true);
            keypad.SetActive(false);
            solved = true;
        }
        else
        {
            input.inputText = "";
            incorrect.Play();
        }
    }
    public void Exit()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        player.GetComponent<FirstPersonController>().enabled = true;
        player.GetComponent<FirstPersonController>().SetMouseCursor(true);
        keypad.SetActive(false);
    }
}

