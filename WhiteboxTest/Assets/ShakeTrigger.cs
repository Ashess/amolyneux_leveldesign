﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeTrigger : MonoBehaviour {

    public TriggerListener_Com trigger;
    public CameraShake cameraShake;
    public AudioSource roar;
    bool used = false;

    private void OnTriggerEnter(Collider trigger)
    {
        if (used == false)
        {
            cameraShake.duration = 2f;
            cameraShake.shouldShake = true;
            roar.Play();
            used = true;
        }
        
    }


}
