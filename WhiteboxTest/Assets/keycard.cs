﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class keycard : MonoBehaviour {
    public Image cursorImage;
    public TriggerListener_Com keycardTrigger;
    public MeshRenderer keyMesh;
    public BoxCollider keyCol;
    public bool keycardCollected;

    void OnMouseOver()
    {
        if (keycardTrigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;
            }
        }
        else
        {
            cursorImage.enabled = false;
        }
    }
    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;

        }

    }
    void OnMouseDown()
    {
        keyMesh.enabled = false;
        keyCol.enabled = false;
        keycardCollected = true;
    }
}
