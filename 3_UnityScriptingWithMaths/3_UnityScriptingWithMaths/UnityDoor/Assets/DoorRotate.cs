﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorRotate : MonoBehaviour
{
    public GameObject door;
    GameObject myPlayer;
    public float targetRot = 90f;
    Vector3 closedRot;
    bool doorOpen;
    public float speed = 1f;
    public MouseListen mouse;
    bool inTrigger;
    bool click;
    public Image cursorImage;
    

    void Start()
    {
        myPlayer = GameObject.FindGameObjectWithTag("Player");
        closedRot = door.transform.localRotation.eulerAngles;
    }

    void Update()
    {
        if (inTrigger)
        {
            if (mouse.mouseCursorOn == true)
            {
                if (cursorImage.enabled == false)
                {
                    cursorImage.enabled = true;

                }
            }
            else
            {
                cursorImage.enabled = false;
            }


        }
        if (inTrigger && mouse.mouseClick && click == false)
        {
            click = true;
            DoorInteract();
        }
        else if (mouse.mouseClick == false)
        {
            click = false;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            inTrigger = true;
        } 
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            inTrigger = false;
            cursorImage.enabled = false;
        }

    }

    void DoorInteract()
    {
        Vector3 finishRot;
        if (doorOpen != true)
        {
            Vector3 playerDirection = door.transform.position - myPlayer.transform.position;
            float dot = Vector3.Dot(playerDirection, transform.forward);
            Debug.Log(dot);
            doorOpen = true;
            if (dot > 0)
            {
                finishRot = new Vector3(closedRot.x, closedRot.y + targetRot, closedRot.z);
            }
            else
            {
                // door.transform.localRotation = Quaternion.Euler(new Vector3(0, -90, 0));
                finishRot =  new Vector3(closedRot.x, closedRot.y - targetRot, closedRot.z);
            }
        }
        else
        {
            finishRot = closedRot;
            doorOpen = false;
        }
        StopCoroutine("DoorMotion");
        StartCoroutine("DoorMotion", finishRot);
   
    
    }
    IEnumerator DoorMotion(Vector3 target)
    {
        while (Quaternion.Angle(door.transform.localRotation, Quaternion.Euler(target)) >= 0.02f)
        {
            door.transform.localRotation = Quaternion.Slerp(door.transform.localRotation, Quaternion.Euler(target), speed * Time.deltaTime);
            yield return null;
        }

        door.transform.localRotation = Quaternion.Euler(target);
        yield return null;
    }

}
