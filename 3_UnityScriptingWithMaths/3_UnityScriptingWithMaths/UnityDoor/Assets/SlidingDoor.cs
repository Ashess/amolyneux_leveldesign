﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingDoor : MonoBehaviour
{
    public Transform door1Tran;
    public Transform door2Tran;
    public float openAmount = 1f;
    public float speed = 1f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            //door1Tran.localPosition = new Vector3(openAmount, 0, 0);
            // door2Tran.localPosition = new Vector3(-openAmount, 0, 0);
            StopCoroutine("doorMove");
            StartCoroutine("doorMove", openAmount);

        }


    }
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            // door1Tran.localPosition = Vector3.zero;
            // door2Tran.localPosition = Vector3.zero;
            StopCoroutine("doorMove");
            StartCoroutine("doorMove", 0f);
        }

    }

    IEnumerator doorMove(float target)
    {
        float xPos = door1Tran.localPosition.x;
        while (xPos < (target - 0.02f) || xPos > (target + 0.02f))
        {
            xPos = Mathf.Lerp(door1Tran.localPosition.x, target, speed * Time.deltaTime);
            door1Tran.localPosition = new Vector3(xPos, 0, 0);
            door2Tran.localPosition = new Vector3(-xPos, 0, 0);
            yield return null;
        }
        door1Tran.localPosition = new Vector3(target, 0, 0);
        door2Tran.localPosition = new Vector3(-target, 0, 0);
        yield return null;
    }
}