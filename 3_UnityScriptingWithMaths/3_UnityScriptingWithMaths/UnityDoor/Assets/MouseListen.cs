﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseListen : MonoBehaviour
{
    public bool mouseCursorOn;
    public bool mouseClick;

    void OnMouseDown()
    {
        mouseClick = true;  
    }

    void OnMouseUp()
    {
        mouseClick = false;
    }

    void OnMouseOver()
    {
        if (mouseCursorOn == false)
        {
            mouseCursorOn = true;
        }

    }

    void OnMouseExit()
    {
        mouseCursorOn = false;
    }
}
